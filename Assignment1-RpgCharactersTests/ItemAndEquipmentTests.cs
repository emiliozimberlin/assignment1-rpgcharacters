﻿using assignment1_rpgcharacters;
using assignment1_rpgcharacters.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Assignment1_RpgCharactersTests
{
    /// <summary>
    /// Testing item system.
    /// </summary>
    public class ItemAndEquipmentTests

    {
        #region TestItemsAndCharacter
        Warrior warrior = new("Warrior");
        Weapon testAxe = new()
        {
            Name = "Common axe",
            Level = 1,
            ItemSlot = Item.Slot.SLOT_WEAPON,
            Type = Weapon.WeaponType.WEAPON_AXE,
            WeaponAttributes = new WeaponAttribute() { BaseDamge = 7, AttackSpeed = 1.1 }
        };

        Armor testPlateBody = new()
        {
            Name = "Common plate body armor",
            Level = 1,
            ItemSlot = Item.Slot.SLOT_BODY,
            Type = Armor.ArmorType.ARMOUR_PLATE,
            Attributes = new PrimaryAttribute() {Strength = 1 }
        };
        Weapon testBow = new()
        {
            Name = "Common bow",
            Level = 1,
            ItemSlot = Item.Slot.SLOT_WEAPON,
            Type = Weapon.WeaponType.WEAPON_BOW,
            WeaponAttributes = new WeaponAttribute() { BaseDamge = 12, AttackSpeed = 0.8 }
        };
        Armor testClothHead = new()
        {
            Name = "Common cloth head armor",
            Level = 1,
            ItemSlot = Item.Slot.SLOT_BODY,
            Type = Armor.ArmorType.ARMOUR_CLOTH,
            Attributes = new PrimaryAttribute() { Intelligence = 5 }
        };
        Weapon testSword = new()
        {
            Name = "Common Sword",
            Level = 1,
            ItemSlot = Item.Slot.SLOT_WEAPON,
            Type = Weapon.WeaponType.WEAPON_SWORD,
            WeaponAttributes = new WeaponAttribute() { BaseDamge = 10, AttackSpeed = 1.1 }
        };
        #endregion
        #region EquipItem
        [Fact]
        public void EquipItem_EquipingATooHighLevelWeaponOnWarrior_ShuoldThrowAInvalidWeaponException()
        {
            // Arrange
            string expected = "Can't Use Weapon Due to Incorrect Level Requirements or weapon type is wrong";
            // Act
            testAxe.Level = 2;
            var exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
            string actual = exception.Message;
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void EquipItem_EquipingATooHighLevelArmorOnWarrior_ShuoldThrowAInvalidArmorException()
        {
            // Arrange
            string expected = "Can't Use Armor Due to Incorrect Level Requirements or armor type is wrong";
            // Act
            testPlateBody.Level = 2;
            var exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
            string actual = exception.Message;
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void EquipItem_EquipingAWrongTypeOfWeaponOnWarrior_ShuoldThrowAInvalidWeaponException()
        {
            // Arrange
            string expected = "Can't Use Weapon Due to Incorrect Level Requirements or weapon type is wrong";
            // Act
            
            var exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
            string actual = exception.Message;
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void EquipItem_EquipingATypeOfArmorOnWarrior_ShuoldThrowAInvalidArmorException()
        {
            // Arrange
            string expected = "Can't Use Armor Due to Incorrect Level Requirements or armor type is wrong";
            // Act
            var exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothHead));
            string actual = exception.Message;
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void EquipItem_EquipingAValidWeaponOnWarrior_ShuoldReturnVaildWeaponMessage()
        {
            // Arrange
            string expected = "New weapon equipped!";
            // Act
            string actual = warrior.EquipItem(testAxe);
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void EquipItem_EquipingAValidArmorOnWarrior_ShuoldReturnVaildArmorMessage()
        {
            // Arrange
            string expected = "New armor equipped!";
            // Act
            string actual = warrior.EquipItem(testPlateBody);
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void EquipItem_EquipingFirstOneWeaponThenAnotherWeaponOnWarrior_ShuoldReturnTheWeaponNameOfTheLastWeaponEquipt()
        {
            // Arrange
            string expected = "Common Sword";
            // Act
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testSword);
            string actual = warrior.Equipment[Item.Slot.SLOT_WEAPON].Name;
            // Assert
            Assert.Equal(expected, actual);

        }
        #endregion
        #region CharacterDamage
        [Fact]
        public void CharacterDamage_CalculatesDPSWithNoWeaponEquipt_ShuoldReturnDps()
        {
            // Arrange
            double expected = 1 * (1+(5/100));
            // Act
            double actual = warrior.CharacterDamage();
            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CharacterDamage_CalculatesDPSWithWeaponEquipt_ShuoldReturnDps()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + (5 / 100));
            // Act
            warrior.EquipItem(testAxe);
            double actual = warrior.CharacterDamage();
            // Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CharacterDamage_CalculatesDPSWithWeaponEquiptAndArmorEquipt_ShuoldReturnDps()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            // Act
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);
            double actual = warrior.CharacterDamage();
            // Assert
            Assert.Equal(expected, actual);

        }
        #endregion
    }
}
