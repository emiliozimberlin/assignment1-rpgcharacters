using System;
using Xunit;
using assignment1_rpgcharacters;

namespace Assignment1_RpgCharactersTests
{
    /// <summary>
    /// Testing characters attributes and leveling system.
    /// </summary>
    public class CharacterAttributeAndLevelTests
    {
        #region Constructor
        [Fact]
        public void Constructor_CreatingANewCharacter_ShouldStartAtLevelOne()
        {
            // Arrange
            Warrior warrior = new("Joe");
            int expected = 1;
            // Act

            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelingACharacterFromOneToTwo_ShouldLevelUptoLeveLTwo()
        {
            // Arrange
            Warrior warrior = new("Joe");
            int expected = 2;
            // Act
            warrior.LevelUp();
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Constructor_CreatingWarriorClass_ShouldReturnTheCorrectDefaultAttributes()
        {
            // Arrange
            Warrior warrior = new("Warrior");

            PrimaryAttribute expected = new() { Strength = 5, Dexterity = 2, Intelligence = 1 };
            // Act
            PrimaryAttribute actual = warrior.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Constructor_CreatingMageClass_ShouldReturnTheCorrectDefaultAttributes()
        {
            // Arrange
            Mage mage = new("Mage");
            PrimaryAttribute expected = new() { Strength = 1, Dexterity = 1, Intelligence = 8 };
            // Act
            PrimaryAttribute actual = mage.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Constructor_CreatingRougeClass_ShouldReturnTheCorrectDefaultAttributes()
        {
            Rouge rouge = new("Rogue");

            PrimaryAttribute expected = new() { Strength = 2, Dexterity = 6, Intelligence = 1 };
            // Act
            PrimaryAttribute actual = rouge.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Constructor_CreatingRangerClass_ShouldReturnTheCorrectDefaultAttributes()
        {
            // Arrange

            Ranger ranger = new("Ranger");

            PrimaryAttribute expected = new() { Strength = 1, Dexterity = 7, Intelligence = 1 };
            // Act
            PrimaryAttribute actual = ranger.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        #region LevelUp
        [Fact]
        public void LevelUp_CreatingAWarriorClassAndLevelingUp_ShouldReturnTheCorrectIncreasedAttributes()
        {
            // Arrange
            Warrior warrior = new("Warrior");

            PrimaryAttribute expected = new() { Strength = 8, Dexterity = 4, Intelligence = 2 };
            // Act
            warrior.LevelUp();
            PrimaryAttribute actual = warrior.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_CreatingAMageClassAndLevelingUp_ShouldReturnTheCorrectIncreasedAttributes()
        {
            // Arrange
            Mage mage = new("Mage");

            PrimaryAttribute expected = new() { Strength = 2, Dexterity = 2, Intelligence = 13 };
            // Act
            mage.LevelUp();
            PrimaryAttribute actual = mage.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_CreatingARougeClassAndLevelingUp_ShouldReturnTheCorrectIncreasedAttributes()
        {
            // Arrange
            Rouge rouge = new("Rouge");

            PrimaryAttribute expected = new() { Strength = 3, Dexterity = 10, Intelligence = 2 };
            // Act
            rouge.LevelUp();
            PrimaryAttribute actual = rouge.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_CreatingARangerClassAndLevelingUp_ShouldReturnTheCorrectIncreasedAttributes()
        {
            // Arrange
            Ranger ranger = new("Ranger");

            PrimaryAttribute expected = new() { Strength = 2, Dexterity = 12, Intelligence = 2 };
            // Act
            ranger.LevelUp();
            PrimaryAttribute actual = ranger.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}

