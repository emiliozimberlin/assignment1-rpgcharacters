﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Armor type.
    /// </summary>
    public class Armor : Item
    {
        public ArmorType Type { get; set; }
        public PrimaryAttribute Attributes { get; set; }
        /// <summary>
        /// List of armor types.
        /// </summary>
        public enum ArmorType 
        {
            ARMOUR_CLOTH,
            ARMOUR_LEATHER,
            ARMOUR_MAIL,
            ARMOUR_PLATE
        }
        // Constructor

        // behaviour
    }
}
