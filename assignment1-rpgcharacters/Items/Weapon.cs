﻿using assignment1_rpgcharacters.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Weapon type.
    /// </summary>
    public class Weapon : Item
    {
        // State
        public WeaponAttribute WeaponAttributes { get; set; }
        public WeaponType Type { get; set; }
        /// <summary>
        /// List of Weapon types.
        /// </summary>
        public enum WeaponType 
        {
            WEAPON_AXE,
            WEAPON_BOW,
            WEAPON_DAGGER,
            WEAPON_HAMMER,
            WEAPON_STAFF,
            WEAPON_SWORD,
            WEAPON_WAND
        }

        // Constructor

        // behaviour

    }
}
