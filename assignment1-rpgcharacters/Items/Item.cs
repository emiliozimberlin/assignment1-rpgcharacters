﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Abstract class of items
    /// </summary>
    public abstract class Item
    {
        // State
        public string Name { get; set; }
        public int Level { get; set; }
        public int PrimaryAttributes { get; set; }
        public Slot ItemSlot { get; set; }
        /// <summary>
        /// List of items slots.
        /// </summary>
        public enum Slot
        {
            SLOT_HEAD,
            SLOT_BODY,
            SLOT_LEGS,
            SLOT_WEAPON
        } 
        // Constructor

        // behaviour
    }
}
