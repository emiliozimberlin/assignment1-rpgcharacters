﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters.Helpers
{
    /// <summary>
    /// Holds the weapon attributes.
    /// </summary>
    public class WeaponAttribute
    {
        public int BaseDamge { get; set; }
        public double AttackSpeed { get; set; }
    }
}
