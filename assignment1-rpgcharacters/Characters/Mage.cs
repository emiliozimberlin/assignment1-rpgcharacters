﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Mage Class.
    /// </summary>
    public class Mage : Character
    {
        /// <summary>
        /// Initialize a Mage with starting stats
        /// </summary>
        /// <param name="name">Name of Mage.</param>
        public Mage(string name) : base(name,1,1,8)
        {
            TotalPrimaryAttributes = BasePrimaryAttributes;
        }

        public override bool CanWearArmorType(Armor.ArmorType armor)
        {
            if (armor == Armor.ArmorType.ARMOUR_CLOTH)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override bool CanUseWeaponType(Weapon.WeaponType weapon)
        {
            if (weapon == Weapon.WeaponType.WEAPON_STAFF || weapon == Weapon.WeaponType.WEAPON_WAND)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override void LevelUp()
        {
            //str 1, dex 1, int 5
            base.Level++;
            PrimaryAttribute UpdatedPrimaryAttribute = new() { Strength = 1, Dexterity = 1, Intelligence = 5 };
            BasePrimaryAttributes += UpdatedPrimaryAttribute;
            TotalPrimaryAttributes += UpdatedPrimaryAttribute;

        }
        /// <inheritdoc/>
        public override double CharacterDamage()
        {
            double dps = CalculateweaponDPS(); 
            return dps * (1 + (TotalPrimaryAttributes.Intelligence / 100));
        }
    }
}
