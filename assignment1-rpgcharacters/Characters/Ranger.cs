﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Ranger Class.
    /// </summary>
    public class Ranger : Character
    {
        /// <summary>
        /// Initialize a Ranger with starting stats
        /// </summary>
        /// <param name="name">Name of Ranger.</param>
        public Ranger(string name) : base(name,1,7,1)
        {
            TotalPrimaryAttributes = BasePrimaryAttributes;
        }
        /// <inheritdoc/>
        public override bool CanWearArmorType(Armor.ArmorType armor)
        {
            if (armor == Armor.ArmorType.ARMOUR_MAIL || armor == Armor.ArmorType.ARMOUR_LEATHER)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override bool CanUseWeaponType(Weapon.WeaponType weapon)
        {
            if (weapon == Weapon.WeaponType.WEAPON_BOW)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override void LevelUp()
        {
            //str 1, dex 5, int 1
            base.Level++;
            PrimaryAttribute UpdatedPrimaryAttribute = new() { Strength = 1, Dexterity = 5, Intelligence = 1 };
            BasePrimaryAttributes += UpdatedPrimaryAttribute;
            TotalPrimaryAttributes += UpdatedPrimaryAttribute;

        }
        /// <inheritdoc/>
        public override double CharacterDamage()
        {
            double dps = CalculateweaponDPS();
            return dps * (1 + (TotalPrimaryAttributes.Dexterity / 100));
        }
    }
}
