﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Warrior Class.
    /// </summary>
    public class Warrior : Character
    {
        /// <summary>
        /// Initialize a Warrior with starting stats
        /// </summary>
        /// <param name="name">Name of Warrior.</param>
        public Warrior(string name) : base(name,5,2,1)
        {

            TotalPrimaryAttributes = BasePrimaryAttributes;
        }
        /// <inheritdoc/>
        public override bool CanWearArmorType(Armor.ArmorType armor)
        {
            if (armor == Armor.ArmorType.ARMOUR_MAIL || armor == Armor.ArmorType.ARMOUR_PLATE)
                return true;
            else
                return false;

        }
        /// <inheritdoc/>
        public override bool CanUseWeaponType(Weapon.WeaponType weapon)
        {
            if (weapon == Weapon.WeaponType.WEAPON_AXE || weapon == Weapon.WeaponType.WEAPON_HAMMER || weapon == Weapon.WeaponType.WEAPON_SWORD)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override void LevelUp()
        {
            //str 3, dex 2, in 1
            base.Level++;
            PrimaryAttribute UpdatedPrimaryAttribute = new() {Strength = 3, Dexterity = 2, Intelligence = 1 };
            BasePrimaryAttributes += UpdatedPrimaryAttribute;
            TotalPrimaryAttributes += UpdatedPrimaryAttribute;
  

        }
        /// <inheritdoc/>
        public override double CharacterDamage()
        {
            double dps = CalculateweaponDPS();
            return dps * (1 + (TotalPrimaryAttributes.Strength / 100));
        }

    }
}
