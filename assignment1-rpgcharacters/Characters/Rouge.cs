﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Rouge Class.
    /// </summary>
    public class Rouge : Character
    {
        /// <summary>
        /// Initialize a Rouge with starting stats
        /// </summary>
        /// <param name="name">Name of Rouge.</param>
        public Rouge(string name) : base(name,2,6,1)
        {
            TotalPrimaryAttributes = BasePrimaryAttributes;
        }
        /// <inheritdoc/>
        public override bool CanWearArmorType(Armor.ArmorType armor)
        {
            if (armor == Armor.ArmorType.ARMOUR_MAIL || armor == Armor.ArmorType.ARMOUR_LEATHER)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override bool CanUseWeaponType(Weapon.WeaponType weapon)
        {
            if (weapon == Weapon.WeaponType.WEAPON_DAGGER || weapon == Weapon.WeaponType.WEAPON_SWORD)
                return true;
            else
                return false;
        }
        /// <inheritdoc/>
        public override void LevelUp()
        {
            //str 1, dex 4, in 1
            base.Level++;

            PrimaryAttribute UpdatedPrimaryAttribute = new() { Strength = 1, Dexterity = 4, Intelligence = 1 };
            BasePrimaryAttributes += UpdatedPrimaryAttribute;
            TotalPrimaryAttributes += UpdatedPrimaryAttribute;

        }
        /// <inheritdoc/>
        public override double CharacterDamage()
        {
            double dps = CalculateweaponDPS();
            return dps * (1 + (TotalPrimaryAttributes.Dexterity / 100));
        }
    }
}
