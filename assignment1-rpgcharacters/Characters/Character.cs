﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Abstract class for all Characters
    /// </summary>
    public abstract class Character
    {

        // State
        public string Name { get; set; }
        public int Level { get; set; } = 1;

        public PrimaryAttribute TotalPrimaryAttributes { get; set; }
        public PrimaryAttribute BasePrimaryAttributes { get; set; }
        public Dictionary<Item.Slot, Item> Equipment { get; set; } = new();
        public PrimaryAttribute PrimaryAttributes { get; set; }

        // Constructor

        /// <summary>
        /// Initialize a character.
        /// </summary>
        /// <param name="name">name of character</param>
        /// <param name="strength">characters strength</param>
        /// <param name="dexterity">characters dexterity</param>
        /// <param name="intelligence">characters intelligences</param>
        public Character(string name, int strength, int dexterity, int intelligence)
        {
            Name = name;
            BasePrimaryAttributes = new PrimaryAttribute() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence };

        }



        // behavior

        /// <summary>
        /// Levels up a character and updates characters stats.
        /// </summary>
        public abstract void LevelUp();
        /// <summary>
        /// Checks of the character can meets the weapon requirements.
        /// </summary>
        /// <param name="weapon"> witch type of weapon it is (Sword, Wand, Axe, etc).</param>
        /// <returns>True if character can use it, else false.</returns>
        public abstract bool CanUseWeaponType(Weapon.WeaponType weapon);
        /// <summary>
        /// Checks of the character can meets the armor requirements.
        /// </summary>
        /// <param name="armor"> witch type of armor it is (cloth, leather, mail, and plate)</param>
        /// <returns>True if character can use it, else false.</returns>
        public abstract bool CanWearArmorType(Armor.ArmorType armor);
        /// <summary>
        /// Calculates the characters damage.
        /// </summary>
        /// <returns>The result of calculation as a double.</returns>
        public abstract double CharacterDamage();
        /// <summary>
        /// Calculates the weapons damage per second that the character has equip.
        /// </summary>
        /// <returns>The result of calculation as a double</returns>
        public double CalculateweaponDPS() 
        {
            if (Equipment.ContainsKey(Item.Slot.SLOT_WEAPON))
            {
                Weapon currentWeapon = (Weapon)Equipment[Item.Slot.SLOT_WEAPON];
                double dps = currentWeapon.WeaponAttributes.BaseDamge * currentWeapon.WeaponAttributes.AttackSpeed;
                return dps;
            }
            else
                return 1;
        }
        /// <summary>
        /// Equips either a weapon or a armor.
        /// </summary>
        /// <param name="item">Item thats being equip. </param>
        /// <returns>Method either EquipArmor or EquipWeapon</returns>
        public string EquipItem(Item item)
        {
            if (item is Armor armor) 
            {
                return EquipArmor(item.ItemSlot ,armor);
            }

            Weapon weapon = (Weapon)item;
            return EquipWeapon(item.ItemSlot, weapon);
            

        }
        /// <summary>
        /// Equips a weapon.
        /// </summary>
        /// <param name="slot">which slot the items gets equip.</param>
        /// <param name="weapon">Which type of weapon it is.</param>
        /// <returns>A string or throws a exception.</returns>
        public string EquipWeapon(Item.Slot slot, Weapon weapon) 
        {
            if (Equipment.ContainsKey(slot))
                Equipment.Remove(slot);

            if (!CanUseWeaponType(weapon.Type) || (Level < weapon.Level))
                throw new InvalidWeaponException("Can't Use Weapon Due to Incorrect Level Requirements or weapon type is wrong");

            Equipment.Add(slot, weapon);
            return "New weapon equipped!";

        }
         /// <summary>
         /// Equips a armor.
         /// </summary>
         /// <param name="slot">which slot the armor gets equip.</param>
         /// <param name="weapon">Which type of armor it is.</param>
         /// <returns>A string or throws a exception.</returns>
        public string EquipArmor(Item.Slot slot, Armor armor) 
        {
            
            if (!CanWearArmorType(armor.Type) || (Level < armor.Level))
                throw new InvalidArmorException("Can't Use Armor Due to Incorrect Level Requirements or armor type is wrong");
         
            PrimaryAttribute armorAttributes = new() { Strength = armor.Attributes.Strength, Dexterity = armor.Attributes.Dexterity, Intelligence = armor.Attributes.Intelligence };
            TotalPrimaryAttributes = BasePrimaryAttributes + armorAttributes;
            Equipment.Add(slot, armor);
            return "New armor equipped!";

        }/// <summary>
        /// Shows all the stats of the character
        /// </summary>
        /// <returns>A string containing all the characters stats</returns>
        public override string ToString()
        {
            StringBuilder sb = new();
            sb.Append("Class: " + this.GetType().Name + "\n").
            Append("Name: " + Name + "\n").
            Append("Level: " + Level + "\n").
            Append("Strength: " + TotalPrimaryAttributes.Strength + "\n"). 
            Append("Dexterity: " + TotalPrimaryAttributes.Dexterity + "\n").
            Append("Intelligence: " + TotalPrimaryAttributes.Intelligence + "\n").
            Append("Damage: " + CharacterDamage());

            return sb.ToString();
        }

    }
}
