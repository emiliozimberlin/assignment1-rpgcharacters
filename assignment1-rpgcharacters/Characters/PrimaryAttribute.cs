﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Holds the primary attributes of a character.
    /// </summary>
    public class PrimaryAttribute
    {

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// sums two attributes together
        /// </summary>
        /// <param name="lhs">Lest attribute</param>
        /// <param name="rhs">Right attribute</param>
        /// <returns>The sum of the calculation.</returns>
        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs) => new()
        {
            Strength = lhs.Strength + rhs.Strength,
            Dexterity = lhs.Dexterity + rhs.Dexterity,
            Intelligence = lhs.Intelligence + rhs.Intelligence
        };
        /// <summary>
        /// Check if PrimaryAttribute are equal.
        /// </summary>
        /// <param name="obj"> PrimaryAttribute object it compares.</param>
        /// <returns>True if equal else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is PrimaryAttribute attributes && Strength == attributes.Strength && Dexterity == attributes.Dexterity && Intelligence == attributes.Intelligence)
                return true;
            else
                return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }

}
