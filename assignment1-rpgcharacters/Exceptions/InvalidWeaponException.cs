﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{

    /// <summary>
    /// Invalid weapon exception.
    /// </summary>
    public class InvalidWeaponException : Exception
    {

        public InvalidWeaponException(string message)
        : base(message)
        {

        }
        public InvalidWeaponException(string message, Exception innerException)
        : base(message, innerException)
        {

        }
    }
}
