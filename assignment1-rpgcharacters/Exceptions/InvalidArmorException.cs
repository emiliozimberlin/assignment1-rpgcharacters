﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1_rpgcharacters
{
    /// <summary>
    /// Invalid armor exception.
    /// </summary>
    public class InvalidArmorException : Exception
    {

        public InvalidArmorException(string message)
        : base(message)
        {
           
        }
        public InvalidArmorException(string message, Exception innerException)
        : base(message, innerException) 
        {

        }




    }
}
