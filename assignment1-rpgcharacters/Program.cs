﻿using assignment1_rpgcharacters.Helpers;
using System;

namespace assignment1_rpgcharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new("Warrior");
            Weapon testAxe = new () 
            {   Name = "Common axe",
                Level = 1,
                ItemSlot = Item.Slot.SLOT_WEAPON,
                Type = Weapon.WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttribute() { BaseDamge = 7, AttackSpeed = 1.1 }
            };
            warrior.LevelUp();
       
            Console.WriteLine(warrior);

        }
    }
}
